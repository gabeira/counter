package com.counterwear.counter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.wearable.view.WatchViewStub;
import android.view.View;
import android.widget.TextView;
import android.os.Vibrator;

public class MyActivityWear extends Activity {

    private TextView mTextView;
    private TextView counterTextView;
    private int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_activity_wear);
        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                mTextView = (TextView) stub.findViewById(R.id.text);
                counterTextView = (TextView) stub.findViewById(R.id.number);
               // mTextView.setText("Counter");

                stub.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                     plus();
                    }
                });

            }
        });


    }

   public void plus(){

       count++;

       counterTextView.setText(String.valueOf(count));

       Vibrator v = (Vibrator) getApplication().getSystemService(Context.VIBRATOR_SERVICE);
        //Vibrate for 500 milliseconds;
       v.vibrate(500);
   }
}
